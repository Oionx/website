// Comments supplied by 0NeXt on 10/5/23

var totalBoops = 0; // Clicks in total
var boops = 0; // Current clicks
var level = 1; // Current level
var boopsPerBoop = 1; // The amound of extra clicks given at every given click
var boopsUntilLevelUp = 5; // How many clicks until level up
var level_dual = 0

document.getElementById("boopsUntilLevelUp").innerHTML = boopsUntilLevelUp;

function update_elements() { // Updates Current HTML elements
    document.getElementById("boopsCurrent").innerHTML = boops;
    document.getElementById("totalBoops").innerHTML = totalBoops;
    document.getElementById("boopsUntilLevelUp").innerHTML = boopsUntilLevelUp;
    document.getElementById("lizardLevel").innerHTML = level;

}

function beardie_clicked() { // Updates data for a single click
    boops += boopsPerBoop;
    totalBoops += boopsPerBoop; 
    if(boops >= boopsUntilLevelUp) { // if clicks are greater than or equal to levelup amount 
        boops = 0; 
        boopsUntilLevelUp *= 2; // Doubles amount required to level up
        level++;
    }

    update_elements();
}

function shop(){
    alert("Shop Menu Coming Soon!"); // Shop comming soon
}

function upgrades() {
    alert("Upgrades Menu Coming Soon!"); // Upgrades comming soon
}

