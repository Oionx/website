const messages = document.getElementById("chat-messages");
const apiKey = ("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");

document.getElementById("send_btn").addEventListener("click", send_message);

function send_message(){
  event.preventDefault();
  
  const user_message = document.getElementById("chat-input").value;
  alert(user_message);

  messages.innerHTML += `<div class="message user-message">
  <img src="./user.svg" alt="user"> <span>${user_message}</span>
  </div>`;

  
  const response = await axios.post(
    "https://api.openai.com/v1/completions",
    {
      prompt: user_message,
      model: "text-davinci-003",
      temperature: 0,
      max_tokens: 1000,
      top_p: 1,
      frequency_penalty: 0.0,
      presence_penalty: 0.0,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${apiKey}`,
      },
    }
  );
  const chatbot_response = response.data.choices[0].text;

  messages.innerHTML += `<div class="message bot-message">
  <img src="./chatbot.svg" alt="chatbot"> <span>${chatbot_response}</span>
  </div>`;
}


